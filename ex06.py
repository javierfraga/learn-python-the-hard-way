x = ("There are %d types of people." % 10)
binary = "binary"
do_not = "don't"
y = ("Those who know %s and those who %s." % (binary, do_not))

print (x)
# %r difference between %s
print ("I said: %r." % x) # notice no, ''
print (y)
print ("I also said: '%s'." % y) # has ''

hilarious = False
joke_evaluation = "Isn't that joke so funny?! %r"

print (joke_evaluation % hilarious)
