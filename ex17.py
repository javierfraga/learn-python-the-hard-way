from sys import argv
from os.path import exists

script, from_file, to_file = argv

print ('Copying from %s to %s' % (from_file, to_file))


indata = open(from_file).read()

print ('The input file is%d bytes long' % len(indata))

print ('does the output file exists? %r' % exists(to_file))
print ('Ready, hit RETURN to continue, CTRL-C to abort.')
input()

open(to_file, 'w').write(indata)

print ('Alright, all done')

# do not need to close file if opened with one-liner
# close(from_file)
# close(to_file)
